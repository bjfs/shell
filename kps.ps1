# Wrapper script for KeePass Script by bjfs on 2014-05-03
#
# (avoids setting %PATH% when KP is not installed in portable mode;
#  has nothing to do with Single Command Operation)
#
# You need: 1) KeePass 2) KPScript plug-in in KeePass directory
# Get the plug-in from http://keepass.info/plugins.html#kpscript
#
# In case the script doesn't want to be executed, set this as PS admin:
# & Set-ExecutionPolicy Bypass

# Safe default:
$kps_proc_path = "C:\Program Files (x86)\KeePass Password Safe 2\KPScript.exe"

# The script to be run, acquired from command-line argument:
$kps_file_path = $args[0]

# Magic:
& $kps_proc_path $kps_file_path
