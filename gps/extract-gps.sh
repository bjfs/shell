#/bin/bash

# Taken from: https://exiftool.org/forum/index.php?topic=10540.0
# Notice sometimes there are empty files as output, for some reason this works better:
# exiftool -config gpsdatetime.config -p gpx.fmt -ee /mnt/[...]/Garmin/GRMN0026.MP4 > out.gpx 2>/dev/null
# Also, the gpsdatetime settings are not most fortunate (yet)

exiftool -config gpsdatetime.config -p gpx.fmt -ee $1 > out.gpx
