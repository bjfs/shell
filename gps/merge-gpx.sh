#!/bin/bash
# Original idea: stackoverflow.com/questions/38554131/merge-all-gpx-files-within-a-folder-into-one-file-with-gpsbabel

# Notice you still have to merge the tracks and combine segments for a clean path...
# This can be done in GPS Editor software or maybe there are more options in gpsbabel?

cd /mnt/c/Users/Bart/Documents
ff=""
for f in *.gpx
do
    ff="$ff -f $f"
done
gpsbabel -i gpx $ff -o gpx -F "All.gpx"
