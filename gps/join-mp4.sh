#!/bin/bash

# Ideas from https://superuser.com/questions/521113/join-mp4-files-in-linux
# MP4Box scenario works best with gpac package being in version 2 (or later)

filesList=""
for file in $(ls *.MP4|sort -n);do
    filesList="$filesList -cat $file"
done
MP4Box $filesList -new merged_files_$(date +%Y%m%d_%H%M%S).mp4
