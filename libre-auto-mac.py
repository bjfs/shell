#!/Applications/LibreOffice.app/Contents/Resources/python
import uno
import os

## Author: bjfs & Gemini 2.0 Flash
## Credits: The proper Java object in UNO URL for modern LibreOffice was mentioned in
## https://medium.com/@vaklinov81/automating-document-creation-with-python-and-libreoffice-a-step-by-step-guide-3282abffc1f2
##
## Notice that in order for this script to work, you have to run LibreOffice like this:
## /Applications/LibreOffice.app/Contents/MacOS/soffice --accept="socket,host=localhost,port=2002;urp;StarOffice.ComponentContext"
## In cases where you want automation without UI, add also --headless

localContext = uno.getComponentContext()
resolver = localContext.ServiceManager.createInstanceWithContext("com.sun.star.bridge.UnoUrlResolver", localContext)
connectStr = "uno:socket,host=localhost,port=2002;urp;StarOffice.ComponentContext"  # Match the port!
try: 
    remoteContext = resolver.resolve(connectStr)
except Exception as e:
    print("Can't connect, did you run soffice with proper UNO URL in command line?")
    exit(1)

try:
    sm = remoteContext.ServiceManager
    desktop = sm.createInstanceWithContext("com.sun.star.frame.Desktop", remoteContext)
    model = desktop.loadComponentFromURL("file:///Users/USERNAME/FOLDER/FILE.odb", "_blank", 0, [])
except Exception as e:
    import traceback  # Import the traceback module
    traceback.print_exc() # Print the full traceback
    print(f"Full Error: {e}")  # Print the exception object itself
    exit(2)
