#!/usr/bin/perl

# set-ddns.pl
#
# Copyright (c) 05-20-2006, JRS System Solutions
# Copyright (c) 15-08-2012, Shlomi Fish
# Copyright (c) 12-10-2014, B.J. Stobiecki

# All rights reserved under standard BSD license
# details: http://www.opensource.org/licenses/bsd-license.php

# note: requires BIND 9.3.1 and CPAN LWP::UserAgent, HTTP::Request, and HTTP::Response
#       FreeBSD admins may find the CPAN modules under /usr/ports/www/p5-libwww
#
# WARNING: FreeBSD admins must make CERTAIN they are calling the BIND9 version
#          of nsupdate - FreeBSD systems have a nasty habit of leaving a copy
#          of the BIND8 version higher up in the PATH, even in systems shipped
#          with BIND9 in the base install!
#
# Updated to adhere to Modern Perl conventions (see
# http://perl-begin.org/tutorials/bad-elements/ ) by Shlomi Fish
# ( http://www.shlomifish.org/ ).

use strict;
use warnings;

use LWP::UserAgent;
use HTTP::Request;
use HTTP::Response;

my $NAMESERVER = 'ns.your-own-bind.net.';
my $KEYDIR = '/usr/home/ddns';
my $KEYFILE = 'Kdynamic.host.com.+123+31337.private';
my $TYPE = 'A';
my $TTL = '10';
my $HOST = 'dynamic.host.com';
my $url_string = 'http://host.with-your-ip-detector.org/ipx.php';
my $ua = LWP::UserAgent->new;
my $req = HTTP::Request->new('GET',$url_string);
my $resp = $ua->request($req)->as_string();

my $WAN;
if (not
    (
    ($WAN) =
    $resp =~ m/Current IP Address\: (\d{0,3}\.\d{0,3}\.\d{0,3}\.\d{0,3}).*/s
    )
    )
{
    die "Cannot match Current IP Address in response.";
}

chdir ($KEYDIR);

my $ipfile = "/tmp/myip";
my $savedip = do {
    local $/ = undef;
    open (my $fh, "<", $ipfile) or die "could not open $ipfile: $!";
    <$fh>;
};

if ($savedip eq $WAN) {
 exit; # when WAN IP is the same as found before, suspend operations
};

open (my $fh, ">", $ipfile) or die "could not open $ipfile: $!";
print $fh $WAN; # this will print new WAN IP into the file handler
close $fh or die "could not close $ipfile: $!";

print "==============================\nNew WAN IP found: $WAN\n==============================\n";
chdir ($KEYDIR);
open (my $nsupdate_fh, "| /usr/local/bin/nsupdate -k $KEYFILE -v")
    or die "Cannot open nsupdate! $!";
print {$nsupdate_fh} "server $NAMESERVER\n";
print {$nsupdate_fh} "update delete $HOST A\n";
print {$nsupdate_fh} "update add $HOST $TTL A $WAN\n";
print {$nsupdate_fh} "show\n";
print {$nsupdate_fh} "send\n";
close ($nsupdate_fh) or die "Cannot close nsupdate - $!.";
