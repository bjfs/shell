* **libre-auto-mac.py** - example of LibreOffice automation using UNO on MacBooks
* **parse-ekg2-logsqlite.sh**	- a log parser for the sqlite logger for a Polish IM named EKG2
* **select-x.sh** - simple dialog which I've used on FreeBSD to select WM during console login
* **set-ddns.pl** - a slightly modified DDNS updater for homebrew solutions in Perl (needs own DNS server, also)
* **gps/extract-gps.sh** - extracts GPS coordinates from MP4 files used in navigation cameras into GPX format
* **gps/join-mp4.sh** - merges MP4 files, this also keeps GPS data if present
* **gps/merge-gpx.sh** - merges GPX coordinates (in scenario when you don't need a merged video)
