#!/bin/sh
#
# Parser for ekg2 logs based on sqlite3
# v0.3 by bjfs
 
if [ "$1" != "" ]
then
 if [ -e $1 ]
  then
  sqlite3 $1 'select * from log_msg;' | awk -F'|' '
  {
   if ($5 == '1')
     printf("\033[33;01m<<\033[0m ");
   else
     printf("\033[32;01m>>\033[m ");
 
   printf("[%s", strftime("%Y.%m.%d ",$6));
   printf("%s] ", strftime("%H:%M:%S",$6));
   printf("%s ", $3);
 
   if (NF > 8)
    {
       i = 8;
       do {
          printf("%s|", $i);
          i++;
       } while (i <= NF-1)
       printf("%s\n", $NF);
    }
    else printf("%s\n", $8);
  }'
  else
 echo "ERROR: File not found!"
 exit 2
 fi
else
echo "ERROR: No log file name given!"
exit 1
fi
