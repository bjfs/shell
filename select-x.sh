#!/bin/sh

if [ "$TERM" = jfbterm-color ]; then
exit
fi

dialog --clear --backtitle "CHOOSE YOUR POISON" \
    --menu "Use [UP/DOWN] key to move" 12 60 6 \
    "XFCE4"    "To start Xfce 4" \
    "XWM"      "To start XWM" \
    "JFB"      "To use CLI with framebuffer" \
    "EXIT"     "Return to CLI without framebuffer" 2> /tmp/menuchoices.$$

    retopt=$?
    choice=`cat /tmp/menuchoices.$$`
    rm /tmp/menuchoices.$$

    case $retopt in

           0) case $choice in

                  XFCE4) startxfce4 ;;
                  XWM)   startx ;;
                  JFB)   jfbterm ;;
                  EXIT)  clear; exit 0;;

              esac ;;

          *) clear ; exit ;;
    esac
